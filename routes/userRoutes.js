const express = require("express");
const router = express.Router();

// controllers
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

// check if email exists
router.post("/check-email", (req, res) => {
  userController.checkEmailExists(req.body).then((result) => res.send(result));
});

// user registration
router.post("/register", (req, res) => {
  userController.register(req.body).then((result) => res.send(result));
});

// login
router.post("/login", (req, res) => {
  userController.login(req.body).then((result) => res.send(result));
});

// logged in. Retrieving your own data.
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile(userData.id).then((result) => res.send(result));
});

router.post("/enroll", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId,
  };
  userController.enroll(data).then((result) => res.send(result));
});

module.exports = router;
