const express = require("express");
const router = express.Router();
const auth = require("./../auth");

// controllers
const courseController = require("./../controllers/courseControllers");

// Retrieve active courses
router.get("/active", (req, res) => {
  courseController.getAllActive().then((result) => res.send(result));
});

router.get("/all", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  courseController.getAllCourse().then((result) => res.send(result));
});

// Add course
router.post("/add", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  courseController.addCourse(req.body).then((result) => res.send(result));
});

// Retrieve one course
router.get("/:courseId", auth.verify, (req, res) => {
  const userDate = auth.decode(req.headers.authorization);
  courseController
    .getCourse(req.params.courseId)
    .then((result) => res.send(result));
});

router.put("/:courseId/edit", auth.verify, (req, res) => {
  courseController
    .updateCourse(req.params.courseId, req.body)
    .then((result) => res.send(result));
});

router.put("/:courseId/archive", auth.verify, (req, res) => {
  courseController
    .archiveCourse(req.params.courseId, req.body)
    .then((result) => res.send(result));
});

router.put("/:courseId/unarchive", auth.verify, (req, res) => {
  courseController
    .unarchiveCourse(req.params.courseId)
    .then((result) => res.send(result));
});

router.delete("/:courseId/delete", auth.verify, (req, res) => {
  courseController
    .deleteCourse(req.params.courseId)
    .then((result) => res.send(result));
});

module.exports = router;
