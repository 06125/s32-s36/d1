const Course = require("./../models/Courses");

module.exports = {
  getAllCourse: () => {
    return Course.find({}).then((result) => result);
  },
  getAllActive: () => {
    return Course.find({ isActive: true }).then((result) => result);
  },

  addCourse: (reqBody) => {
    /* let { name, description, price } = reqBody;
    let newCourse = new Course({
      name: name,
      description: description,
      price: price,
    }); */

    let newCourse = new Course(reqBody);
    return newCourse.save().then((result, error) => (error ? false : true));
  },

  // Added by myself --------- 09/05 -------------
  getCourse: (courseId) => {
    return Course.findById(courseId).then((result) => result);
  },

  updateCourse: (courseId, updates) => {
    // console.log(updates);
    return Course.findByIdAndUpdate(courseId, updates, { new: true }).then(
      (result, error) => (error ? error : result)
    );
  },

  archiveCourse: (courseId) => {
    return Course.findByIdAndUpdate(courseId, { isActive: false }).then(
      (result, error) => (error ? false : true)
    );
  },

  unarchiveCourse: (courseId) => {
    return Course.findByIdAndUpdate(courseId, { isActive: true }).then(
      (result, error) => (error ? false : true)
    );
  },

  deleteCourse: (courseId) => {
    return Course.findByIdAndDelete(courseId).then((result, error) =>
      error ? false : true
    );
  },
};
