const User = require("./../models/User");
const Course = require("./../models/Courses");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports = {
  checkEmailExists: (reqBody) => {
    return User.find({ email: reqBody.email }).then((result) => {
      if (result.length !== 0) return true;
      else return false;
    });
  },

  register: (reqBody) => {
    let { firstName, lastName, mobileNo, email, password } = reqBody;
    let newUser = new User({
      firstName: firstName,
      lastName: lastName,
      mobileNo: mobileNo,
      email: email,
      password: bcrypt.hashSync(password, 10),
    });

    /* let newUser = new User({
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      mobileNo: reqBody.mobileNo,
      email: reqBody.email,
      password: bcrypt.hashSync(reqBody.password, 10),
    }); */

    return newUser.save().then((result, error) => {
      if (error) {
        return error;
      } else {
        return true;
      }
    });
  },

  login: (reqBody) => {
    return User.findOne({ email: reqBody.email }).then((result) => {
      // if (!result) return false;
      if (bcrypt.compareSync(reqBody.password, result?.password || "")) {
        return { access: auth.createAccessToken(result.toObject()) };
      } else {
        return false;
      }
    });
  },

  getProfile: (id) => {
    return User.findById(id).then((result) => {
      result.password = "********";
      return result;
    });
  },

  enroll: async (data) => {
    const userSaveStatus = await User.findById(data.userId).then((user) => {
      user.enrollments.push({
        courseId: data.courseId,
      });

      return user.save().then((user, error) => (error ? false : true));
    });

    const courseSaveStatus = await Course.findById(data.courseId).then(
      (course) => {
        course.enrollees.push({
          userId: data.userId,
        });

        return course.save().then((course, error) => (error ? false : true));
      }
    );

    if (userSaveStatus && courseSaveStatus) {
      return true;
    } else {
      return false;
    }
  },
};
