const mongoose = require("mongoose");

const coursesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name of the course is required."],
  },
  description: {
    type: String,
    required: [true, "Description is required."],
  },
  price: {
    type: Number,
    required: [true, "Price is required."],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createOn: {
    type: Date,
    default: new Date(),
  },
  enrollees: [
    {
      userId: {
        type: String,
        required: [true, "userId is required."],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model("Course", coursesSchema);
