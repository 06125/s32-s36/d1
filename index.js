const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
app = express();
const PORT = process.env.PORT || 3000;

// routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

mongoose
  .connect(
    "mongodb+srv://eclespinosa:emilmongo@cluster0.vtyel.mongodb.net/course_booking?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Connected to database."))
  .catch((error) => console.log(error));

// every request - routes
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

app.listen(PORT, () => console.log(`Sever running on port ${PORT}.`));

/* https://rocky-wildwood-85934.herokuapp.com/ | https://git.heroku.com/rocky-wildwood-85934.git */
