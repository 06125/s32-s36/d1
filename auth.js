/* 
We will use this for authentication
    Login
    Retrieve user details 

JSON WEB TOKENS
    METHODS:
        sign(<data from the client>, <secret>, {options}) - creates a token
        verify(<token>, <secret>, CBFn()) - check if the token is present
        decode(<token>, {options}).payload - interpret or decodes the created token.
*/

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports = {
  // Creating a token
  createAccessToken: (user) => {
    const data = {
      id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
    };
    return jwt.sign(data, secret);
  },

  // Verify token
  verify: (req, res, next) => {
    let token = req.headers.authorization;
    // if token is not undefined
    if (token) {
      token = token.slice(7);

      return jwt.verify(token, secret, (error, data) => {
        if (error) {
          return res.send({ auth: "failed" });
        } else {
          next(); // proceed to the next argument in the userRoutes.
        }
      });
    }
  },

  // Decode token
  decode: (token) => {
    if (token) {
      token = token.slice(7);
      return jwt.decode(token, { complete: true }).payload;
    }
  },
};
